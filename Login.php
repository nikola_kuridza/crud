<?php

class Login extends Init {

    private $secret;

    private $password;

    public $first;

    function __construct($config, $translations) {
        $this->baseUrl   = $config['main']['baseUrl'];
        $this->table     = 'login';
        $this->secret    = $config['main']['secret'];
        $this->password  = $config['main']['password'];
        $this->first     = array_keys($config['tables'])[0];
        $this->trans     = $translations->trans;
        $this->language  = $translations->language;
        $this->languages = $translations->languages;
        $this->pageName  = $this->trans->signIn;
    }

    public function login(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if(hash('sha512', $_POST['password'] . $this->secret) == $this->password){
                $_SESSION['logged'] = true;
            } else {
                $this->setFlash($this->trans->badPass);
            }
            $this->redirect('', null, true);
        }
        $this->load();
    }

    public function logout(){
        session_unset();
        $this->redirect('', null, true);
    }

}