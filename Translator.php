<?php

class Translator {

    static function getTranslations($source, $language){
        $translations    = parse_ini_file($source, true);
        $response['languages'] = array_keys($translations[array_keys($translations)[0]]);
        if(in_array($language, $response['languages'])){
            $response['language']  = $language;
            $response['trans']     = (object) array_map(function($val) use ($language){
                return isset($val[$language]) ? $val[$language] : null;
            }, $translations);
            return (object)$response;
        } else {
           header('Location: ?' . $response['languages'][0]);
        }
    }
}