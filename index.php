<?php
session_start();

/* Class Loader */
spl_autoload_register(function ($class) {
    include $class . '.php';
});

/* Load configuration */
$c = new DIContainer();

$config  = parse_ini_file('config.ini', true);

$c->db = function ($c) use ($config) {
    return new PDO('mysql:host=' . $config['database']['host'] . ';dbname=' . $config['database']['name'], $config['database']['user'], $config['database']['pass']);
};

if($config['main']['environment'] === 'debug')
    $c->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
unset($config['database']);

/* Router */
$lang   = @explode('/',array_keys($_GET)[0])[0];
$table  = @explode('/',array_keys($_GET)[0])[1];
$action = @explode('/',array_keys($_GET)[0])[2];
$id     = @explode('/',array_keys($_GET)[0])[3];

$translations = Translator::getTranslations($config['main']['translationFile'], $lang);
if(! isset($_SESSION['logged']) || ! $_SESSION['logged'] || $table === 'logout') {
    $lg = new Login($config, $translations);
    $table === 'logout' ? $lg->logout() : $lg->login();
} else {
    $cnt = new Controller($c->db, $table, $config, $translations);
    if(! in_array($table, array_keys($config['tables']))) {
        $cnt->notFound($table);
    } elseif(method_exists($cnt, $action)) {
        $cnt->{$action}($id);
    } else {
        $cnt->notFound($action);
    }
}