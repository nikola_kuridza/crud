<form action="<?php echo $this->router('create'); ?>" method="post">
    <?php echo $this->trans->deleted; ?>: <?php echo implode(' ', $data); ?>
    <?php foreach($data as $key => $row): ?>
        <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $row; ?>">
    <?php endforeach; ?>
    <input type="submit" class="btn btn-primary pull-right" value="<?php echo $this->trans->undo; ?>">
</form>