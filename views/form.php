<?php if(!empty($this->files[0])): ?>
    <img src="<?php if(isset($data)) echo $data[$this->files[0]]; ?>" class="img-thumbnail">
<?php endif; ?>
<form action="<?php echo $this->router($action, isset($id) ? $id : null); ?>"
      method="post"
      <?php if(!empty($this->files[0])): ?>
          enctype="multipart/form-data"
      <?php endif; ?>
>
    <div class="form-group">
        <?php foreach($columns as $column): ?>
            <?php if($column === 'id') continue; ?>
            <?php if(in_array($column, $this->files)): ?>
                <input type="file" name="<?php echo $column; ?>" id="file">
            <?php else: ?>
                <input type="text"
                       class="form-control"
                       name="<?php echo $column; ?>"
                       placeholder="<?php echo $column; ?>"
                       value="<?php if(isset($data)) echo $data[$column] ?>"
                >
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <input type="submit" class="btn btn-primary" value="<?php echo $this->trans->submit; ?>">
</form>