<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap-theme.min.css">
    <link href="http://getbootstrap.com/examples/sticky-footer/sticky-footer.css" rel="stylesheet">
</head>
<body>
<div id="wrap">
    <div class="navbar navbar-inverse">
        <ul class="nav navbar-nav">
            <li class="active"><a>CRUD</a></li>
            <?php $save = $this->table; ?>
            <?php foreach($this->tables as $table): ?>
                <?php $this->table = $table; ?>
                <li><a href="<?php echo $this->router('read');   ?>"><?php echo $this->table; ?></a> </li>
                <li><a href="<?php echo $this->router('create'); ?>"><?php echo $this->trans->add; ?> <?php echo $this->table; ?></a></li>
            <?php endforeach; ?>
            <?php $this->table = $save; ?>
        </ul>
        <?php if($this->logged): ?>
            <form action="<?php echo $this->router('search'); ?>" method="post" class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" name="search" placeholder="<?php echo $this->trans->search; ?>" value="<?php echo $this->search; ?>">
                </div>
                <button type="submit" class="btn btn-default">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </form>
        <?php endif; ?>
    </div>
    <div class="container">
        <div class="starter-template">
            <h1><?php echo $this->pageName; ?></h1>
       </div>
    </div>
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <?php if(isset($_SESSION['flash'])): ?>
                <div class="alert alert-info">
                    <?php echo $_SESSION['flash']; unset($_SESSION['flash']); ?>
                </div>
            <?php endif; ?>
            <div>
                <?php include('views/' . $view . '.php'); ?>
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <div class="container">
        <p class="text-muted credit">
            <?php echo date('d/m/Y'); ?>
            <?php echo implode('|', array_map(function($language){
                return '<a href="?' . $language . '">' . $language . '</a>';
            },$this->languages)); ?>
            <?php if($this->logged): ?>
                <a class="pull-right" href="<?php echo $this->router('logout', null, true); ?>">
                    <span class="glyphicon glyphicon-off"></span> <?php echo $this->trans->logout; ?>
                </a>
            <?php endif; ?>
        </p>
    </div>
</div>


<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
</body>
</html>