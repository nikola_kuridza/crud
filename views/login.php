<form class="form-signin" action="<?php echo $this->router(''); ?>" method="post">
  <input name="password" type="password" class="form-control" placeholder="<?php echo $this->trans->password; ?>">
  <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo $this->trans->submit; ?></button>
</form>
