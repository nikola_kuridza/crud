<div class="list-group">
    <?php foreach($data as $row): ?>
        <div class="list-group-item" >
            <a href="<?php echo $this->router('update', $row['id']); ?>">
                <?php foreach($row as $key => $data): ?>
                    <?php if($key == 'id' || in_array($key, $this->files)) continue; ?>
                    <?php echo $data; ?>
                <?php endforeach; ?>
            </a>
        <a class="pull-right" title="<?php echo $this->trans->delete; ?>" href="<?php echo $this->router('delete', $row['id']); ?>">
            <span class="glyphicon glyphicon-remove"></span>
        </a>
        </div>
    <?php endforeach; ?>
</div>
